; Drush Make file to getting the brightcove recorder module up and running quickly

core = 6.x
api = 2

projects[pressflow][type] = "core"
projects[pressflow][download][type] = "get"
projects[pressflow][download][url] = "http://files.pressflow.org/pressflow-6-current.tar.gz"

projects[admin][version] = 2.0
projects[admin][subdir] = "contrib"

projects[brightcove][version] = "2.1"
projects[brightcove][subdir] = "contrib"

projects[cck][version] = 2.9
projects[cck][subdir] = "contrib"

projects[ctools][version] = 1.8
projects[ctools][subdir] = "contrib"

projects[jquery_ui][version] = 1.4
projects[jquery_ui][subdir] = "contrib"

project[imagecache][version] = 6.x-2.0-beta10
project[imagecache][subdir] = "contrib"

project[imageapi][version] = 6.x-1.9
project[imageapi][subdir] = "contrib"

projects[jquery_update][version] = "2.0-alpha1"
projects[jquery_update][subdir] = "contrib"

projects[jquery_ui][version] = "1.5"
projects[jquery_ui][subdir] = "contrib"

projects[lightbox2][version] = 1.11
projects[lightbox2][subdir] = "contrib"

projects[modalframe][version] = "1.7"
projects[modalframe][subdir] = "contrib"

projects[video_recorder][version] = "1.0"
projects[video_recorder][subdir] = "contrib"

libraries[jquery-ui][download][type] = "get"
libraries[jquery-ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery-ui-1.7.3.zip"
libraries[jquery-ui][directory_name] = "jquery.ui"
libraries[jquery-ui][destination] = "modules/contrib/jquery_ui"

libraries[lib-bcmapi][download][type] = "get"
libraries[lib-bcmapi][download][url] = "https://github.com/downloads/BrightcoveOS/PHP-MAPI-Wrapper/BrightcoveOS-PHP-MAPI-Wrapper-2.0.5.zip"
libraries[lib-bcmapi][directory_name] = "bcmapi"
